<?php

namespace console\controllers;


use console\jobs\ParentJob;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class JobController extends Controller
{
    /**
     * Generate N events
     * @param $cycleCount int Count of event
     * @return int
     */
    public function actionIndex(int $cycleCount = 1)
    {
        $groups = [
            'queueKasko',
            'queueLoan',
            'queueOsago',
            'queueRef'
        ];

        Yii::beginProfile("Start generating {$cycleCount} events", 'jobs');

        for ($i = 0; $i < $cycleCount; $i++) {
            $group = $groups[mt_rand(0, 3)];
            $id = Yii::$app->redis->incr("$group.message_id");
            Yii::$app->redis->hset("$group.messages", $id, "300;O:22:\"console\jobs\ParentJob\":2:{s:4:\"name\";s:".strlen($group).":\"$group\";s:5:\"index\";i:2;}");
            Yii::$app->redis->lpush("$group.waiting", $id);
        }

        Yii::endProfile("Finish generating {$cycleCount} events", 'jobs');

        return ExitCode::OK;
    }
}