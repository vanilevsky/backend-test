<?php

use yii\queue\redis\Queue;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'queueKasko',
        'queueLoan',
        'queueOsago',
        'queueRef'
    ],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
        ],
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => null,
            'migrationNamespaces' => [
                'yii\queue\db\migrations',
            ],
        ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info', 'trace', 'profile'],
                    'logFile' => '@runtime/logs/jobs/' . date('Y') . '-' . date('m') . '/' . date('d') . '.log',
                    'maxFileSize' => '5120',
                    'maxLogFiles' => 40,
                    'categories' => [
                        'jobs'
                    ],
                    'logVars' => [],
                ],
            ],
        ],
        'queueKasko' => [
            'class' => Queue::class,
            'redis' => 'redis',
            'channel' => 'queueKasko',
        ],
        'queueLoan' => [
            'class' => Queue::class,
            'redis' => 'redis',
            'channel' => 'queueLoan',
        ],
        'queueOsago' => [
            'class' => Queue::class,
            'redis' => 'redis',
            'channel' => 'queueOsago',
        ],
        'queueRef' => [
            'class' => Queue::class,
            'redis' => 'redis',
            'channel' => 'queueRef',
        ],
    ],
    'params' => $params,
];
