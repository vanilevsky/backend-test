<?php

namespace console\jobs;

use Yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;

/**
 * Base job
 * @package console\jobs
 */
class ParentJob extends BaseObject implements JobInterface
{
    const SLEEP_TIME = 2;

    public $name;
    public $index;

    public function execute($queue)
    {
        sleep(self::SLEEP_TIME);
        Yii::info("{$this->name} #{$this->index} finished", 'jobs');
    }
}