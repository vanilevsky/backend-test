# backend-test

Есть скрипт, который генерирует 10 000 событий в секунду. 
Например это событие "Новая заявка".

Необходимо выполнить задачи на основе всех этих событий не дольше чем 
за 10 минут.

Обработка задачи:
1. Обработчик засыпает на 2 секунды.
2. Добавляет запись в файл log.txt о выполнении задачи

События генерируются случайным способом, и относятся к разным группам.
Примеры групп:
1. Заявка на кредит
2. Заявка на страхование КАСКО
3. Заявка на страхование ОСАГО
4. Заявка на рефинансирование

Обработчик должен выполнять все события группами.

# Моё решение

1. Генерируем события из консоли `yii job 10000`, события с разными группами встают в очередь на исполнение   
1. Ручной запуск обработки для всех событий был бы таким. Каждый - новым процессом.

    `/usr/bin/php /var/www/backend-test/yii queue-loan/listen -v`
    
    `/usr/bin/php /var/www/backend-test/yii queue-osago/listen -v`
    
    `/usr/bin/php /var/www/backend-test/yii queue-kasko/listen -v`
    
    `/usr/bin/php /var/www/backend-test/yii queue-ref/listen -v`
    
1. Но нам нужно справиться с объёмом и это распараллелить. Расчитываем нужное количество процессов для наших объёмов и на сервере с помощью Supervisor настраиваем запуск обработчиков очереди для каждой группы:
    
    Пример для одной группы:
    ```
    [program:yii-queue-worker-loan]
    process_name=%(program_name)s_%(process_num)02d
    command=/usr/bin/php /var/www/backend-test/yii queue-loan/listen -v
    autostart=true
    autorestart=true
    user=www-data
    numprocs=9
    redirect_stderr=true
    stdout_logfile=/var/www/my_project/log/yii-queue-worker.log
   
1. Если предположить, что у нас 10000 запросов и на каждый уходит по 2 сек, то 4 Supervisor по 9 процессов уложатся в 10 минут.