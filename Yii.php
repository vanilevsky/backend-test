<?php
/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 * https://github.com/samdark/yii2-cookbook/blob/master/book/ide-autocompletion.md
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

abstract class BaseApplication extends yii\base\Application
{
}

/**
 * Class WebApplication
 * Include only Web application related components here
 *
 */
class WebApplication extends yii\web\Application
{
}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 *
 * @property yii\queue\db\Queue $queueKasko
 * @property yii\queue\db\Queue $queueLoan
 * @property yii\queue\db\Queue $queueOsago
 * @property yii\queue\db\Queue $queueRef
 * @property yii\redis\Connection $redis
 */
class ConsoleApplication extends yii\console\Application
{
}